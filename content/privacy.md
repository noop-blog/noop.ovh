+++
author = "Benny S"
title = "Privacy of NOOP"
date = "2021-04-18"
description = "Privacy of NOOP."
tags = [
    "tampermonkey",
    "javascript"
]
categories = [
    "technial",
    "QoL",
    "automation"
]
+++

How NOOP.ovh treats your privacy.
<!--more-->

## Preface

This is not a official privacy policy of NOOP.ovh.

However, it is still a accurate description of how your privacy is handled by NOOP.ovh.

## Summary

Everyone's privacy is well-respected.

If I am a user, I will trust this site will respect my privacy.

## Information Collected

### GitLab

GitLab is the 3rd Party service provider of NOOP.ovh.

All NOOP.ovh is hosted by GitLab Pages.

No data can be collected by me in this section.

You should refer to their [Privacy Policy](https://about.gitlab.com/privacy/)

Technically, they can collect the following items by server log:

* Your IP address
* Your Datetime of visiting
* Your request URL, cookie (if any), and anything your browser sent
* All internet packets, that means:
  * Your HTTP requests, including
    * User-Agent
    * All other headers
  * Your TCP/UDP packets

#### Note on CDN

If GitLab uses external CDN provider, CDN can also access all items above, except:
* HTTP headers, if CDN is not a SSL Termination Proxy

### NOOP.ovh

NOOP.ovh does track you in a privacy-respecting method.

[Plausible Analytics](https://plausible.io/privacy), an open source software, is used to track your visit.

I confirm that,

* No personal information is collected
* No information such as cookies is stored in the browser
* No information is shared with, sent to or sold to third-parties
* No information is shared with advertising companies
* No information is mined and harvested for personal and behavioral trends
* No information is monetized

Plausible Analytics does not use cookie nor fingerprint to track.

The method of tracking is described in [their website](https://plausible.io/data-policy).

As a summary, the following algorithm is used.

```
hash(daily_salt + website_domain + ip_address + user_agent)
```

## Transparency and Trust

I believe trust comes from transparency.

* The information from Plausible Analytics is available for public.
  * https://tracking.2isk.in/noop.ovh
* The GitLab repo of this site is available for public.
  * https://gitlab.com/noop-blog/noop.ovh
  * Also licensed under CC BY-SA 4.0, unless otherwise specified

## Side Note

I really dislike tracking and JavaScript.

> With great power comes great responsibility.

NOOP.ovh does not use Google Analytics, which track and sell your data to them.

For me, Plausible is a really great solution. It is just a simple visit counter.

If I want to uniquely identify you, I cannot.

> JavaScript sucks.

This site avoids using template that heavily rely on JS.

In my case, JS usually just slow down the site and waste bandwidth, CPU cycles and all resources.

There is no need for a full-blown UI and *any* JS in fact.

> Code, Command > Comment, Text > Image > > anything else, e.g. UI

This is the site for technical, where the focus is code and command, followed by comment and text and lastly image.

Anything else is a waste of resources.

{{< contact >}}
