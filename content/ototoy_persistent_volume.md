+++
author = "Benny S"
title = "Persistent Volume of OTOTOY"
date = "2021-04-17"
description = "Using Tampermonkey to save audio volume at OTOTOY across reload."
tags = [
    "tampermonkey",
    "javascript"
]
categories = [
    "technial",
    "QoL",
    "automation"
]
+++

Using Tampermonkey to save audio volume at OTOTOY across webpages.
<!--more-->

## Summary

This post:

* using (base) JavaScript + Tampermonkey to save OTOTOY Audio Volume

## Motivation

* OTOTOY does not save the volume of previewing music.
* For every new webpage, volume is set to 100%, too loud for me.

### What is OTOTOY?

[OTOTOY](https://ototoy.jp) is a DRM-free, lose-less Music Online Store of Japanese Music.

[About OTOTOY](https://ototoy.jp/about/index):

> OTOTOY is not a simple store, it is also an online magazine about music.
>
> We've already published interviews and reviews about more than 20,000 different artists. We strive to inform you about the vibrant Japanese music scene and its most unique artists. Our readers are able to listen to an extract of every work discussed about in these articles and buy them.
>
> The albums are available to buy in a variety of formats, all DRM-free: from the simple MP3 and AAC, to WAV, FLAC, ALAC in CD-quality or High-Resolution —24bit/44.1kHz and higher—, to DSD (Direct Stream Digital) files.

## Solution

A simple JavaScript is written to:

* Inject into the OTOTOY webpage, with url started with `https://ototoy.jp/_/default/p/`
* Use previous volume
  * If not found, fallback to `DEFAULT_VOL = 0.1` (10%)
* If the volume is changed, save it

### Source Code

GitLab: https://gitlab.com/noop-blog/ototoy-persistent-volume

License: MIT

## Requirements

* Chrome / Firefox, reasonable up to date
  * Only Firefox is tested
* "Tampermonkey" extension

## Installation

1. Install Browser Extension Tampermonkey
  - For Firefox, install from [addons.mozilla.org](https://addons.mozilla.org/en-US/firefox/addon/tampermonkey/)
  - For other browser, see [official site](https://www.tampermonkey.net/)
2. Copy the following link
  - `https://gitlab.com/noop-blog/ototoy-persistent-volume/-/raw/master/ototoy_persistent_volume.js`
  - ⚠️ Check the file content for security first
3. Open Tampermonkey Dashboard

![Open Tampermonkey Dashboard](open_tm_dashboard.webp)

4. Navigate to "Utilities"
5. Paste the link to "Install from URL" and click "Install"
6. Click "Install" in the new page

![Install script to Tampermonkey](tm_install_script.webp)
![Confirm install script to Tampermonkey](tm_install_script_confirm.webp)

## Verify

### Volume saved across pages

=> Notice the volume is same for all pages.

![](ototoy_1.webp)
![](ototoy_2.webp)

### Script is running

=> Notice the red `1` on the top-right corner of the extension icon.

![](ototoy_full.webp)

{{< contact >}}
