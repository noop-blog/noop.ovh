+++
author = "Benny S"
title = "Q&A of my current job"
date = "2022-10-04"
description = "What makes me love then hate my job"
categories = [
    "non-technial"
]
+++

# Q&A of my current job

## What makes me from loving my job to nothing but hate toward it?

Some background info is needed to explain things.

### What made me happy until 2+ months ago?

* Solving problems!
    - Of _ALL_ problems my senior throws at me
        + I solved ALL of them perfectly
        + AND proudly, for my creativity
* I am needed!
    - Things that I did _is_ useful -- someone will used it!

### What made me unhappy in recent 2+ months?

* Things not matter!
    - Everything is trivial, anyone can do it, even my senior alone can do it
    - Most of the things I built is dead, didn't see production at all
    - I can deny things from my senior
        + IF I can choose to not to do, then it is not important at all
* What/Who am I working for?
    - So many ridiculous things.
    - Am I working for the client? OR _someone_ trying to guess the customer need? <- This frustrates me the most
* Why I receive no credit for so many things/role I did?
    - _Every_ company I interviewed are impressed/surprised for the things I did
* Then I realized:
    - This company/team/department just don't care about technical at all
    - "High level High level High level" is what they want

# Why I am becoming more upset more recently?

## Why?

* Nothing to do, literally
    - I already learn every things I can, every things a DevOps/System en. should know
    - I played 2 mobile games at work, then even grow tired of both
    - I read countless webpage about technology stacks, new software, features, updates
    - I setup a personal k8s with lots of apps on it
    - I got 2 certs partially out of boredom, only costs me 1 week or so to prepare
    - Hell, I even taught my senior to get her own cert, and successfully
* I have run out of things to do
    - Time pass slower and slower as a result
    - Reminded me my "work" in "internship" at China, which I totally hate it. I never hate something so badly.
    - Time in office increasing like the "internship"
* I learn nothing
    - Tried to learn about my job/company, only more and more dislike toward them

## What I did?

* I keep telling my senior about the meaningless of my job
    - Only to find out more and more trivial tasks
    - Sometime I _maybe_ blamed her for this
    - But now, I realized _there's nothing she can do to help me anyway_
    - It was never her fault, the company is to be blamed

## What I did in this week?

* I took many AL
* I actually tried my first beer in my life, in hope of making me happy
    - It is terrible, will never drink again
    - Drink expensive juices are more euphoric
    - Cheap juice beer tastes like fake juice
    - Only gave me headache, which I rarely have
* I went to watch the sunset today (Oct 4, 2022)
    - Ok ish

BUT,

* The second I think about going to office, my mood just tumble, more than stock market
    - Just HATE sitting there, do nothing, wasting my life


## My thoughts about myself

* I am a two edged sword
    - I am excel at doing any technical things
    - But I look for the meaning of doing so
        + I HATE doing meaningless things (at work)
* I fulfill my job, and very well
    - I am still proud of what I have completed
    - I completed everything my senior threw at me
* Maybe I need to become someone, that has the power to put a middle finger to customer for unreasonable things
    - Or just accept work is meaningless