+++
author = "Benny S"
title = "NOOP"
date = "2021-03-08"
description = "Initial Post"
categories = [
    "non-technial"
]
+++

The Initial post of NOOP.
<!--more-->

# NOOP

## Definition

> **NOOP** (pronounced "no op"; short for no operation) is an assembly language instruction, programming language statement, or computer protocol command that does nothing.

From [_NOP (code)_ on Wikipedia](https://en.wikipedia.org/wiki/NOP_(code)).

## Interpreted Meaning

In the context of this site,

* do nothing, in the views of professional
* be professional, in the views of non-professional

That means, this site:

* is nothing for you (as a professional)
* is something for you (as a non-professional)

# License

NOOP.ovh by Beeny is licensed under [CC BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0)

* Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
* ShareAlike — If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original.
